<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\AlumnoModel; //decimos dónde está

/**
 * Description of AlumnoController
 *
 * @author a035604227c
 */
class AlumnoController extends BaseController {
    
    public function formInsertAlumno() {
        $data['title'] = 'Formulario Alta Alumnos';
        return view('alumno/formalta', $data);
    }
    
    //este método será llamado tras rellenar los campos del formulario y enviarlo.
    public function insertAlumno(){
        //Tomar los datos del formulario
        $NIA = $this->request->getPost('NIA'); //es el valor del atributo name del campo input
        $nombre = $this->request->getPost('nombre');
        $apellido1 = $this->request->getPost('apellido1');
        $apellido2 = $this->request->getPost('apellido2');
        $nif = $this->request->getPost('nif');
        $email = $this->request->getPost('email');
        //lo pasamos a un array asociativo
        $alumno_nuevo = [
            'NIA' => $NIA,
            'nombre' => $nombre,
            'apellido1' => $apellido1,
            'apellido2' => $apellido2,
            'nif' => $nif,
            'email' => $email,
        ];
        //comprobamos lo que hemos recibido del formulario
        /*echo '<pre>';
        print_r($alumno_nuevo);
        echo '</pre>';*/
        
        //si trabajamos con la base de datos necesitamos un modelo
        //creamos el modelo que nos interesa
        $alumnoModel = new AlumnoModel();
        //usamos el método insert del modelo para añadir los datos de un alumno
        $alumnoModel->insert($alumno_nuevo);
        //ahora queremos que siga añadiendo nuevos alumnos
        return redirect()->to('alumno/formalta');
    }
    
}
