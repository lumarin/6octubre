<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;


class AlumnoModel extends Model { //Ahora tengo un modelo, si no sería una clase normal sin superpoderes.
    protected $table='alumnos';
    protected $primaryKey = 'id';
    protected $returnType = 'object'; //porque me gusta
    //hemos de decir qué campos son modificables
    protected $allowedFields = ['NIF','nombre','apellido1','apellido2','nif','email'];
    
}