<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of GrupoModel
 *
 * @author a035604227c
 */

namespace App\Models;
use CodeIgniter\Model;


class GrupoModel extends Model {
    /* definimos los parámetros de la tabla a la que
     * queremos acceder
     */
    protected $table='grupos';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    
}
