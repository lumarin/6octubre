<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <div class="container">
        <h1 class="text-primary"><?= $title?></h1>
            <form action="<?= site_url('alumno/alta')?>" method="post">
                <div class=form-group">
                    <label for="NIA">NIA </label>
                    <input type="text" name="NIA" value="" id="NIA" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="NIA">Nombre </label>
                    <input type="text" name="nombre" value="" id="nombre" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="NIA">1er Apellido </label>
                    <input type="text" name="apellido1" value="" id="apellido1" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="NIA">2º Apellido </label>
                    <input type="text" name="apellido2" value="" id="apellido2" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="NIA">NIF </label>
                    <input type="text" name="nif" value="" id="nif" class="form-control"/>
                </div>
                <div class=form-group">
                    <label for="NIA">Correo electrónico </label>
                    <input type="text" name="email" value="" id="email" class="form-control"/>
                </div>
                <input type="submit" name="enviar" value="Enviar" />
            
            </form>
        </div>
    </body>
</html>